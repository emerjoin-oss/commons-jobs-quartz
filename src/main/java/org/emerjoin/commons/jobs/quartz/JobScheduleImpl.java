package org.emerjoin.commons.jobs.quartz;

import org.emerjoin.commons.jobs.JobSchedule;
import org.emerjoin.commons.jobs.JobsProviderException;
import org.quartz.*;

public class JobScheduleImpl implements JobSchedule {

    private TriggerKey triggerKey;
    private Scheduler scheduler;

    JobScheduleImpl(TriggerKey key, Scheduler scheduler){
        this.triggerKey = key;
        this.scheduler = scheduler;
    }

    @Override
    public void change(String expression) throws JobsProviderException {
        if(expression==null||expression.isEmpty())
            throw new IllegalArgumentException("expression must not be null nor empty");
        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(CronScheduleBuilder.cronSchedule(expression))
                .build();
        try {
            this.scheduler.rescheduleJob(triggerKey, cronTrigger);
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error rescheduling job with triggerKey="+triggerKey,
                    ex);
        }
    }
}
