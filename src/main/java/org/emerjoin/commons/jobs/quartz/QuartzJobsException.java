package org.emerjoin.commons.jobs.quartz;

import org.emerjoin.commons.jobs.JobsProviderException;

public class QuartzJobsException extends JobsProviderException {

    public QuartzJobsException(String message) {
        super(message);
    }

    public QuartzJobsException(Throwable cause) {
        super("Unexpected error found",cause);
    }

    public QuartzJobsException(String message, Throwable cause) {
        super(message, cause);
    }
}
