package org.emerjoin.commons.jobs.quartz;

import org.emerjoin.commons.jobs.SchedulerCtl;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class QuartzScheduleCtl implements SchedulerCtl {

    private Scheduler scheduler;

    QuartzScheduleCtl(Scheduler scheduler){
        this.scheduler = scheduler;
    }

    @Override
    public void standby() {
        if(isInStandby())
            throw new IllegalStateException("scheduler already in standby");
        try {
            this.scheduler.standby();
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error putting scheduler in StandBy",
                    ex);
        }
    }

    @Override
    public boolean isInStandby() {
        try {
            return this.scheduler.isInStandbyMode();
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error checking scheduler standby status",
                    ex);
        }
    }

    @Override
    public void resume() {
        if(!isInStandby())
            throw new IllegalStateException("scheduler is not in standby");
        try {
            this.scheduler.start();
        }catch (SchedulerException ex){
            throw new QuartzJobsInternalException("error resuming scheduler",
                    ex);
        }
    }
}
