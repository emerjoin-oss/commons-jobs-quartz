package org.emerjoin.commons.jobs.quartz;

import org.emerjoin.commons.jobs.JobsProviderContext;
import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

public class QuartzJobFactory implements JobFactory {

    private JobsProviderContext context;

    QuartzJobFactory(JobsProviderContext context){
        this.context = context;
    }

    @Override
    public Job newJob(TriggerFiredBundle triggerFiredBundle, Scheduler scheduler) throws SchedulerException {
        Class<? extends Job> jobType = triggerFiredBundle.getJobDetail().getJobClass();
        try {
            AbstractQuartzJob jobInstance = (AbstractQuartzJob) jobType.newInstance();
            jobInstance.setContext(context);
            return jobInstance;
        }catch (Exception ex){
            throw new QuartzJobsInternalException("error instantiating Quartz job type: "+jobType.getCanonicalName(),
                    ex);
        }
    }

}
