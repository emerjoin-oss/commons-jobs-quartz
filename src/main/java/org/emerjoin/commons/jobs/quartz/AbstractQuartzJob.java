package org.emerjoin.commons.jobs.quartz;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.emerjoin.commons.jobs.JobExecutionId;
import org.emerjoin.commons.jobs.JobsProviderContext;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.TriggerKey;

public class AbstractQuartzJob implements Job {

    private Class<? extends org.emerjoin.commons.jobs.Job> jobType;
    private JobsProviderContext context;
    private static final Logger LOGGER = LogManager.getLogger(AbstractQuartzJob.class);

    public AbstractQuartzJob(Class<? extends org.emerjoin.commons.jobs.Job> jobType){
        if(jobType==null)
            throw new IllegalArgumentException("jobType must not be null");
        this.jobType = jobType;
    }

    Class<? extends org.emerjoin.commons.jobs.Job> getJobType(){

        return this.jobType;

    }

    void setContext(JobsProviderContext context){
        this.context = context;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        TriggerKey triggerKey = jobExecutionContext.getTrigger().getKey();
        if(triggerKey.getName().endsWith(QuartzJobsProvider.SCHEDULED_JOBS_TAG)) {
            LOGGER.info("Firing scheduled execution of Job: "+this.jobType.getCanonicalName());
            this.context.executeScheduled(this.jobType, new JobExecutionId(jobExecutionContext.getFireInstanceId()));
        }else{
            JobDataMap jobDataMap = jobExecutionContext.getTrigger().getJobDataMap();
            String executionId = jobDataMap.getString(Constants.EXECUTION_ID);
            LOGGER.info("Executing Job: "+this.jobType.getCanonicalName());
            this.context.execute(this.jobType, new JobExecutionId(executionId),
                    jobDataMap);
        }
    }

}
