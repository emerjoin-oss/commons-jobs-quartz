package org.emerjoin.commons.jobs.quartz;

public class QuartzJobsInternalException extends QuartzJobsException {

    public QuartzJobsInternalException(String message) {
        super(message);
    }

    public QuartzJobsInternalException(Throwable cause) {
        super(cause);
    }

    public QuartzJobsInternalException(String message, Throwable cause) {
        super(message, cause);
    }
}
