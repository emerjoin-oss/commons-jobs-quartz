# Commons-jobs-quartz
A **commons-jobs-api** provider implementation for Quartz

# Maven Coordinates
## Repository
Emerjoin OSS repository
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>maven</name>
    <url>https://pkg.emerjoin.org/oss</url>
</repository>
```

## Artifact
Latest version
```xml
<dependency>
    <groupId>org.emerjoin.commons</groupId>
    <artifactId>commons-jobs-quartz</artifactId>
    <version>1.0.0-final</version>
</dependency>
```

# Custom Configuration
Custom configurations are provided using a CDI producer with a special qualifier:
```java
    public class QuartzConfigurator {
    
        @QuartzConfig
        @Produces
        public Properties customConfig(){
            //TODO: Set and return the configs
        }
    
    }
```